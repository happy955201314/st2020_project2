const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true; 

// 1 - example => ok
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();
})


// 2 => ok
test('[Content] Login page join button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let button = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(button).toBe('Join');
    await browser.close();
})

// 3 - example => ok
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({path: './test/screenshots/login.png'});
    await browser.close();
})

// 4 => ok
test('[Screenshot] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'aaaaa');
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'bbbbb');
    await page.keyboard.press('Enter', {delay: 100}); 
    //await page.waitForNavigation();
    await page.waitFor(400);
    await page.screenshot({ path: './test/screenshots/Welcome.png' });
    await browser.close();
})

// 5 => ok
test('[Screenshot] Error message when you login without user and room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.keyboard.press('Enter', {delay: 100}); 
    //await page.waitForNavigation();

    await page.waitFor(1000);
    await page.screenshot({ path: './test/screenshots/Error.png' });
    await browser.close();
})

// 6 => ok
test('[Content] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'aaaaa');
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'bbbbb');
    await page.keyboard.press('Enter', {delay: 500}); 
    
    await page.waitForSelector('#messages > li:nth-child(1) > div:nth-child(2) > p'); 
    let message = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(1) > div:nth-child(2) > p').innerHTML;
    });

    expect(message).toBe('Hi aaaaa, Welcome to the chat app');

    await browser.close();
})

// 7 - example => ok
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');

    await browser.close();
})

// 8 => ok
test('[Behavior] Type room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(userName).toBe('R1');

    await browser.close();
})

// 9 - example => ok
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R3', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');
    
    await browser.close();
})

// 10
test('[Behavior] Login 2 users', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Edward', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R4', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
    
    const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page2 = await browser.newPage();
    
    await page2.goto(url);

    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R4', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 

    await page2.waitForSelector('#users > ol > li:nth-child(1)');
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });

    await page2.waitForSelector('#users > ol > li:nth-child(2)');
    let member2 = await page.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });

    expect(member).toBe('Edward');
    expect(member2).toBe('Mike');

    await browser.close();
    await browser2.close();
})

// 11 => ok
test('[Content] The "Send" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Edward', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    let button = await page.evaluate(() => {
        return document.querySelector('#message-form > button').innerHTML;
    });

    expect(button).toBe('Send');

    await browser.close();
})

// 12 => ok
test('[Behavior] Send a message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Edward', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R6', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    
    await page.type('#message-form > input[type=text]', 'yo', {delay:500});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#messages > li:nth-child(2) > div:nth-child(2) > p');
    let message = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > p').innerHTML;
    });
    
    expect(message).toBe('yo');
    
    await browser.close();
})

// 13 => ok
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R7', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    
    await page.type('#message-form > input[type=text]', 'Hi', {delay:100});
    await page.keyboard.press('Enter', {delay: 100}); 
    await page.waitFor(2000)

    const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page2 = await browser.newPage();
    await page2.goto(url);

    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R7', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 
    
    await page2.type('#message-form > input[type=text]', 'Hello', {delay:100});
    await page2.keyboard.press('Enter', {delay: 100}); 
    await page2.waitFor(2000);
    
    let user1 = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(1) > h4').innerHTML;
    });
    let message1 = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > p').innerHTML;
    });
    let user2 = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(4) > div:nth-child(1) > h4').innerHTML;
    });
    let message2 = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(4) > div:nth-child(2) > p').innerHTML;
    });
    expect(user1).toBe('John');
    expect(message1).toBe('Hi');
    expect(user2).toBe('Mike');
    expect(message2).toBe('Hello');
    await browser.close();
    await browser2.close();
})

// 14 => ok
test('[Content] The "Send location" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R2', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
    
    let button = await page.$eval('#send-location', (content) => content.innerHTML);
    expect(button).toBe('Send location');

    await browser.close();
})

// 15
test('[Behavior] Send a location message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    const client = await page.target().createCDPSession();

    await client.send('Emulation.setGeolocationOverride', {
        latitude: 23.5,
        longitude: 124,
        accuracy: 100,
    });
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R8', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});

    const context = browser.defaultBrowserContext();
    await context.clearPermissionOverrides();
    await context.overridePermissions(url, ['geolocation']);

    await page.$eval('#send-location', button => button.click());

    await page.waitForSelector('#messages > li:nth-child(2) > div:nth-child(2) > div > iframe');
    let map = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > div > iframe').innerHTML;
    });

    expect(map).toBe('<a href=\"https://www.maps.ie/coordinates.html\">find my coordinates</a>');
    await browser.close();
})
